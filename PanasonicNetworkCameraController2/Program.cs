﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicNetworkCameraController2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize DirectInput
            var directInput = new DirectInput();

            // Find a Joystick Guid
            var joystickGuid = Guid.Empty;

            //foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad,
            //            DeviceEnumerationFlags.AllDevices))
            //    joystickGuid = deviceInstance.InstanceGuid;

            // If Gamepad not found, look for a Joystick
            if (joystickGuid == Guid.Empty)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick,
                        DeviceEnumerationFlags.AllDevices))
                    joystickGuid = deviceInstance.InstanceGuid;

            // If Joystick not found, throws an error
            if (joystickGuid == Guid.Empty)
            {
                Console.WriteLine("No joystick/Gamepad found.");
                Console.ReadKey();
                Environment.Exit(1);
            }

            // Instantiate the joystick
            var joystick = new Joystick(directInput, joystickGuid);

            //Console.WriteLine("Found Joystick/Gamepad with GUID: {0}", joystickGuid);

            // Query all suported ForceFeedback effects
            var allEffects = joystick.GetEffects();
            //foreach (var effectInfo in allEffects)
            //Console.WriteLine("Effect available {0}", effectInfo.Name);

            // Set BufferSize in order to use buffered data.
            joystick.Properties.BufferSize = 128;

            // Acquire the joystick
            joystick.Acquire();

            //Create panasonic controller
            var comm = new PanasonicCommunicator();

            int XPos = 65535 / 2;
            int YPos = 65535 / 2;
            int ZPos = 65535 / 2;

            // Poll events from joystick
            while (true)
            {
                try
                {
                    joystick.Poll();
                    var datas = joystick.GetBufferedData();
                    var stater = joystick.GetCurrentState();
                    //Console.WriteLine("Rotation X: {0}", stater.RotationX);
                    bool changePanTilt = false;
                    bool changeZoom = false;

                    foreach (var state in datas)
                    {
                        if (state.Offset == JoystickOffset.X)
                        {
                            changePanTilt = true;
                            if (!isDeadZone(state.Value)){
                                XPos = state.Value;
                            }
                            else
                            {
                                XPos = 65535 / 2;
                            }
                        }

                        if (state.Offset == JoystickOffset.Y && !isDeadZone(state.Value))
                        {
                            changePanTilt = true;
                            if (!isDeadZone(state.Value))
                            {
                                YPos = state.Value;
                            }
                            else
                            {
                                YPos = 65535 / 2;
                            }
                        }

                        if (state.Offset == JoystickOffset.RotationZ && !isDeadZone(state.Value))
                        {
                            changeZoom = true;
                            if (!isDeadZone(state.Value))
                            {
                                ZPos = state.Value;
                            }
                            else
                            {
                                ZPos = 65535 / 2;
                            }
                            
                        }
                        if (state.Offset == JoystickOffset.Sliders0)
                        {
                            Console.SetCursorPosition(5, 1);
                            Console.Write(state.Value);
                        }
                    }

                    if (changePanTilt)
                    {
                        Console.SetCursorPosition(0, 0);
                        Console.Write("X:" + convertJoy(XPos) + " Y:" + convertJoy(YPos) + "                    ");
                        comm.PanTilt(convertJoy(XPos), convertJoy(YPos));
                    }

                    if (changeZoom)
                    {
                        Console.SetCursorPosition(0, 1);
                        Console.Write("Z:" + convertJoy(ZPos, quint: false, sens: 50) + "                    ");
                        comm.Zoom(convertJoy(ZPos, quint: false, sens: 50));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public static bool isDeadZone(int val)
        {
            int center = 65535 / 2;
            double deadPer = .15;

            var lower = center - (center * deadPer);
            var upper = center + (center * deadPer);

            return !(val > upper || val < lower);
        }

        private static int convertJoy(int joyspeed, bool invert = false, int sens = 100, bool quint = false)
        {
            var center = (65535 / 2);
            var lowerZoneStart = center - (65535 * .15);
            var upperZoneStart = center + (65535 * .15);
            
            if (joyspeed > upperZoneStart)
            {
                var range = 65535 - upperZoneStart;
                var outPreSpeed = (joyspeed - upperZoneStart) / (range * .01) / 100;

                int outSpeed = (int)Math.Round(CubicEaseIn(outPreSpeed) * sens / 2 + 50);
                if (quint)
                    outSpeed = (int)Math.Round(QuinticEaseIn(outPreSpeed) * sens / 2 + 50);

                if (outSpeed > 99)
                {
                    outSpeed = 99;
                }

                return invert ? 99 - outSpeed : outSpeed;
            }
            else if (joyspeed < lowerZoneStart)
            {
                var inverted = -(joyspeed - lowerZoneStart);
                var outPreSpeed = (inverted) / (lowerZoneStart * .01) / 100;


                var outSpeed = -(int)Math.Round(CubicEaseIn(outPreSpeed) * sens / 2 - 50);
                if (quint)
                    outSpeed = -(int)Math.Round(QuinticEaseIn(outPreSpeed) * sens / 2 - 50);        

                if (outSpeed < 1)
                {
                    outSpeed = 1;
                }

                return invert ? 99 - outSpeed : outSpeed;
            }
            else
                return 50;
        }

        static public double CubicEaseIn(double p)
        {
            return p;
        }

        static public double CubicEaseOut(double p)
        {
            return p;
            return -(p * (p - 2));
        }

        /// <summary>
        /// Modeled after the quintic y = x^5
        /// </summary>
        static public double QuinticEaseIn(double p)
        {
            return p * p * p * p * p * p * p;
        }

        /// <summary>
        /// Modeled after the quintic y = (x - 1)^5 + 1
        /// </summary>
        static public double QuinticEaseOut(double p)
        {
            double f = (p - 1);
            return f * f * f * f * f * f * f + 1;
        }
    }
}
