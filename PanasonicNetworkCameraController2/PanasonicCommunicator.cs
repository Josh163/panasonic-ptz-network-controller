﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicNetworkCameraController2
{
    class PanasonicCommunicator
    {       static string ipaddress = "192.168.10.142";
            static string baseUrlFormat = "http://{0}/cgi-bin/aw_ptz?cmd=%23{1}&res={2}";

            static double deadzone = 0.1;

            WebClient client;
            HttpClient hClient;

            public PanasonicCommunicator()
            {
                client = new WebClient();
                hClient = new HttpClient();
            }

            public void PanTilt(int JoySpeedPan, int JoySpeedTilt)
            {
                //joyspeed is 0-65535

                //0-49 left
                //50 stop
                //51-99 right
                
                sendCommand(string.Format("PTS{0}{1}", JoySpeedPan.ToString("D2"), JoySpeedTilt.ToString("D2")));
            }

            public void Zoom(int zoomSpeed)
            {
                sendCommand(string.Format("Z{0}", zoomSpeed.ToString("D2")));
            }

        private void sendCommand(string command)
        {
            try
            {
                Console.SetCursorPosition(0, 4);
                Console.Write(command + "       ");
                //client.DownloadStringAsync(new Uri(string.Format(baseUrlFormat, ipaddress, command, 1)));
                hClient.GetAsync(string.Format(baseUrlFormat, ipaddress, command, 1));
                System.Threading.Thread.Sleep(130);
            }
            catch (Exception e)
            {
                Console.SetCursorPosition(0, 10);
                Console.WriteLine(command);
                Console.SetCursorPosition(0, 11);
                Console.WriteLine(e.Message);
            }
        }
    }
}
